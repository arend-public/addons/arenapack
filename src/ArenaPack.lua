ArenaPack = {}

function printf (fmt, ...)
    DEFAULT_CHAT_FRAME:AddMessage(format(fmt, ...))
end

function Logger (name)
    local li = {}
    li.loggerName = name
    li.log  = function (level, fmt, ...) printf(format("[%s][ArenaPack][%s]: ", level, li.loggerName) .. fmt) end
    li.debug = function (fmt, ...) li.log("DEBUG", fmt, ...) end
    li.info = function (fmt, ...) li.log("INFO", fmt, ...)  end
    li.notice = function (fmt, ...) li.log("NOTICE", fmt, ...) end
    li.warning = function (fmt, ...) li.log("WARNING", fmt, ...) end
    li.error = function (fmt, ...) li.log("ERROR", fmt, ...) end
    li.critical =function (fmt, ...) li.log("CRITICAL", fmt, ...) end

    return li
end

local logger = Logger("main")

ArenaPack.eventHandler = CreateFrame("Frame")
ArenaPack.eventHandler.events = { }

ArenaPack.eventHandler:RegisterEvent("PLAYER_LOGIN")
ArenaPack.eventHandler:RegisterEvent("ARENA_PREP_OPPONENT_SPECIALIZATIONS")

ArenaPack.eventHandler:SetScript("OnEvent", function(self, event, ...)
	if event == "PLAYER_LOGIN" then
		ArenaPack:OnInitialize()
		ArenaPack:OnEnable()
		ArenaPack.eventHandler:UnregisterEvent("PLAYER_LOGIN")
    end
    if "table" == type(self.events) then
		local func = self.events[event]
		if "function" == type(ArenaPack[func]) then
			ArenaPack[func](ArenaPack, event, ...)
		end
	end
end)

function ArenaPack:RegisterEvent(event, func)
	self.eventHandler.events[event] = func or event
	self.eventHandler:RegisterEvent(event)
end

function ArenaPack:UnregisterEvent(event)
	self.eventHandler.events[event] = nil
	self.eventHandler:UnregisterEvent(event)
end

function ArenaPack:UnregisterAllEvents()
	self.eventHandler:UnregisterAllEvents()
end

function ArenaPack:OnInitialize()
    logger.debug("ArenaPack:OnInitialize()")
	self.db = LibStub("AceDB-3.0"):New("ArenaPackDB", ArenaPack:GetDefaultProfile())
    self.db.RegisterCallback(self, "OnProfileChanged", "RefreshConfig")
    self.db.RegisterCallback(self, "OnProfileCopied", "RefreshConfig")
    self.db.RegisterCallback(self, "OnProfileReset", "RefreshConfig")
end

function ArenaPack:OnEnable()
    logger.debug("ArenaPack:OnEnable()")
end

function ArenaPack:RefreshConfig()
    
end

function ArenaPack:GetDefaultProfile()
    local profile = {}
    local defaultModule = {
        enabled = false
    }
    profile.modules = {
        ['**'] = defaultModule
    }

    local defaults = { profile = profile }

    return defaults
end