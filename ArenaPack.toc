## Interface: 100007
## Title: ArenaPack
## Version: $CI_COMMIT_REF_NAME
## Notes: Displays key information while in arena matches.
## Author: Arend Hummeling (arendhum@gmail.com)
## SavedVariables: ArenaPackDB
## OptionalDeps:  Ace3, LibStub, LibSharedMedia-3.0, DRData-1.0

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

src\ArenaPack.lua