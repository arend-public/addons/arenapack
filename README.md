# ArenaPack

<b>Please note: this addon is still a work in progress and should not be used in rated arena before a stable release is made</b>

## Project goal

This project aims to combine the features of several existing addons that are not maintained well enough to be considered production-ready, meaning it's unsafe to use them in the arena.

 